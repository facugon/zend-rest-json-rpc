<?php

class ApiBootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initConfig()
    {
        Zend_Registry::set(
            'config',
            new Zend_Config( $this->getOptions() )
        );
    }

    protected function _initDate()
    {
        date_default_timezone_set(
            Zend_Registry::get('config')->settings->application->datetime
        );
    }
}
