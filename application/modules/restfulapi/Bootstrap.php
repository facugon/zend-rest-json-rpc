<?php

defined('MODULE_BASE_PATH') || define( 'MODULE_BASE_PATH', dirname(__FILE__) );

//require_once 'Controller/Plugin/Auth.php';
require_once 'Controller/Plugin/Route.php';
require_once 'Controller/Plugin/Context.php';
require_once 'Controller/Plugin/Request.php';
require_once 'Controller/Plugin/Response.php';
require_once 'Controller/Plugin/ErrorHandler.php';

//require_once 'Controller/Plugin/Resource/Db.php';
//require_once 'Controller/Plugin/Resource/Config.php';
//require_once 'Controller/Plugin/Resource/Language.php';
//require_once 'Controller/Plugin/Resource/Memcached.php';

/** 
 * Module's bootstrap file. 
 * Notice the bootstrap class' name is 'Modulename_'Bootstrap. 
 * When creating your own modules make sure that you are using the correct namespace 
 */  
class Restfulapi_Bootstrap extends Zend_Application_Module_Bootstrap  
{
    protected function _initPlugins()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('frontcontroller');
        $front = $bootstrap->getResource('frontcontroller');

        xdebug_start_trace( '/tmp/trace.log' );
        $error = new Zend_Controller_Plugin_ErrorHandler() ;
        $error
            ->setErrorHandlerModule('restfulapi')
            ->setErrorHandlerController('error')
            ->setErrorHandlerAction('error');
        $front->registerPlugin($error,99);

        /** numbers set the execution order **/
//        $front->registerPlugin(new Restfulapi_Controller_Plugin_Resource_Config(),2);
//        $front->registerPlugin(new Restfulapi_Controller_Plugin_Resource_Memcached(),3);
//        $front->registerPlugin(new Restfulapi_Controller_Plugin_Resource_Db(),4);
//        $front->registerPlugin(new Restfulapi_Controller_Plugin_Resource_Language(),5);

//        $front->registerPlugin(new Restfulapi_Controller_Plugin_Auth(),6);
        $front->registerPlugin(new Restfulapi_Controller_Plugin_Route());
        $front->registerPlugin(new Restfulapi_Controller_Plugin_Context());
        $front->registerPlugin(new Restfulapi_Controller_Plugin_Response());

        $front->registerPlugin(new Restfulapi_Controller_Plugin_Request());
    }
}
