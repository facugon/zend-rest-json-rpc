<?php

abstract class Restfulapi_Controller_Rest extends Zend_Rest_Controller
{
    const ENV_JSONRPC_V  = '2.0';

    public function init()
    {
        parent::init();

        $action = $this->_getParam('action');

        $context = Zend_Controller_Action_HelperBroker::getStaticHelper('ContextSwitch');
        $context
            ->addActionContext($action,'jsonrpc')
            ->initContext('jsonrpc');

        $this->_helper->viewRenderer->setNoRender(true); // no se renderea la vista automaticamente

        $this->response =& $this->view ;
    }

    //abstract public function headAction() ;
    abstract public function optionsAction() ;
}
