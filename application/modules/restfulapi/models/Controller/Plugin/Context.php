<?php

function initCallback ()
{
    /** nada **/
}

class Restfulapi_Controller_Plugin_Context extends Zend_Controller_Plugin_Abstract
{
    const ENV_JSONRPC_V  = '2.0';

    /** modifico el request para que se entienda Json Rpc **/
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $context = Zend_Controller_Action_HelperBroker::getStaticHelper('ContextSwitch');
        if ( ! $context->hasContext('jsonrpc') )
        {
            $context
                ->setAutoJsonSerialization(false)
                ->setAutoDisableLayout(false)
                ->addContext(
                    'jsonrpc',array(
                        'suffix' => 'jsonrpc' ,
                        'headers' => array(
                            'Content-Type' => 'application/json'
                        ),
                        'callbacks' => array(
                            'init' => 'initCallback'
                        )
                    )
                ) ;
        }
    }
}
