<?php

class Restfulapi_Controller_Plugin_Request extends Zend_Controller_Plugin_Abstract
{
    /** construyo la respuesta adecuada segun el metodo de comunicacion solicitado **/
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        try {

            /** force the early initialization of the Rest Server **/
            $request = Rest_JsonRpc_Server::GetInstance()->getRequest();

        } catch (Exception $e) {

            /** tengo que setear el handler del error manualmente, al estar en el preDispatch el plugin errorHandler no lo captura **/

            // Repoint the request to the default error handler
            $request->setModuleName('default');
            $request->setControllerName('error');
            $request->setActionName('error');

            // Set up the error handler
            $error = new Zend_Controller_Plugin_ErrorHandler();
            $error->type = Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER;
            $error->request = clone($request);
            $error->exception = $e;

            $request->setParam('error_handler', $error);
        }
    }
}
