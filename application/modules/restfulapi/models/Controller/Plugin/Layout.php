<?php

class Restfulapi_Controller_Plugin_Layout extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        //echo "comenzando despacho de layout\n" ;

        if ('restfulapi' != $request->getModuleName()) {
            // If not in this module, return early
            return;
        }

        // Change layout
        Zend_Layout::getMvcInstance()
            ->setLayoutPath(MODULE_PATH . '/views/layouts')
            ->setLayout('jsonrpc');
    }
}
