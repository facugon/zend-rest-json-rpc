<?php

class Restfulapi_Controller_Plugin_Resource_Config extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch (Zend_Controller_Request_Abstract $request)
    {
        $customer = 'fmc' ;

        $front = Zend_Controller_Front::getInstance();
        $bootstrap = $front->getParam('bootstrap');
        $options = $bootstrap->getOptions();

        Zend_Registry::set('application_config', new Zend_Config($options, true));

        Zend_Registry::set('config', new Zend_Config_Ini( CUSTOMERS_PATH . "/{$customer}/data/configs/config.ini", APPLICATION_ENV ));
    }
}
