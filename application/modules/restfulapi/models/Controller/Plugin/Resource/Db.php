<?php

class Restfulapi_Controller_Plugin_Resource_Db extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch (Zend_Controller_Request_Abstract $request)
    {
        $customer = 'fmc' ;

        $front = Zend_Controller_Front::getInstance();
        $bootstrap = $front->getParam('bootstrap');
        $config = $bootstrap->getOptions();

        $options = $config['base']['db'] ;

        $host  = $options['params']['host'] ;
        $pass  = $options['params']['password'] ;

        if ( APPLICATION_ENV == 'development' || APPLICATION_ENV == 'testing' )
            $customer .= '_dev' ;

        $params = array (
            'host'     => $host,
            'password' => $pass,
            'username' => $customer,
            'dbname'   => $customer,
            'charset'  => 'utf8'
        );

        $db = Zend_Db::factory( $options['adapter'], $params );

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $db->setFetchMode(Zend_Db::FETCH_ASSOC);

        Zend_Db_Table::setDefaultAdapter($db);
        Zend_Registry::set('db', $db);
    }
}
