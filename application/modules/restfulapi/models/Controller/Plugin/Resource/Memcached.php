<?php

class Restfulapi_Controller_Plugin_Resource_Memcached extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch (Zend_Controller_Request_Abstract $request)
    {
        $customer = 'fmc' ;

        $front = Zend_Controller_Front::getInstance();
        $bootstrap = $front->getParam('bootstrap');

        $resource = $bootstrap->getPluginResource('memcached');

        $cache = $bootstrap
            ->bootstrap('cachemanager')
            ->getResource('cachemanager')
            ->getCache('memcached');

        $cache->setOption('cache_id_prefix', $customer . '_');

        Zend_Registry::set(
            'memcached', 
            $cache->getBackend()->getMemCacheObj()
        );
    }
}
