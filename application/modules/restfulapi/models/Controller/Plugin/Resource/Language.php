<?php

class Restfulapi_Controller_Plugin_Resource_Language extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $lang     = $this->_request->getParam('lang');
        $registry = Zend_Registry::getInstance();
        $user     = new Zend_Session_Namespace('User');

        $locale   = new Zend_Locale('es_AR');

        if(isset($lang))
        {
            $registry->set('userlanguage', $lang);
            $user->language = $lang;
        }
        elseif(isset($user->language))
            $registry->set('userlanguage', $user->language);
        else 
            $registry->set('userlanguage', $locale->getLanguage().'_'.$locale->getRegion());

        $language = $registry->get('userlanguage');

        $pathCSV = APPLICATION_PATH . "/../data/languages/{$language}.csv" ;

        $custCSV = CUSTOMER_PATH . "/data/languages/{$language}.csv" ;
        if( file_exists($custCSV) )
            $pathCSV = $custCSV ;

        $translate = new Zend_Translate('csv', $pathCSV, $language, array('delimiter' => '='));
        $registry->set('language', $translate);
    }
}
