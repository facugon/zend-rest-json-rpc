<?php

class Restfulapi_Controller_Plugin_Route extends Zend_Controller_Plugin_Abstract
{
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        $front = Zend_Controller_Front::getInstance();
        $rest  = new Zend_Rest_Route(
            $front, array(), array('restfulapi' => array('index','taxi')) 
        );
        $front->getRouter()->addRoute('rest', $rest);
    }
}
