<?php

require_once MODULE_BASE_PATH . '/models/Acl.php' ;

require_once APPLICATION_PATH . '/models/IdentityUtil.php' ;
require_once APPLICATION_PATH . '/models/Usuario.php' ;

class Restfulapi_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch (Zend_Controller_Request_Abstract $request)
    {
        $username = 'staff1' ;
        $password = '123' ;

        $acl  = new Restfulapi_Acl ;
        $auth = Zend_Auth::getInstance() ;
        $auth->setStorage( new Zend_Auth_Storage_Session('Autenticacion') ) ;
        Zend_Registry::set('Autenticacion', $auth) ;

        // do the authentication
        $authAdapter = $this->_initAdapter($username, $password);
        $authResult  = $auth->authenticate($authAdapter);

        if ( $authResult->isValid() )
        {
            // store database row to auth's storage system (Not the password though!)
            $data = $authAdapter->getResultRowObject(null, 'password');
            $auth->getStorage()->write($data);

            $identityUtil = new IdentityUtil;
            $nomcomp = $identityUtil->obtenerNombreCompleto();

            /// Set informacion de la sesion.
            $logindataNamespace = new Zend_Session_Namespace('LoginData');
            $logindataNamespace->nomcomp = $nomcomp;
            $logindataNamespace->nusu = $username;
            $logindataNamespace->loginType = $data->login_type;
            $logindataNamespace->lock();

            $user = new Usuario;
            $user->logLogIn( Zend_Auth::getInstance()->getIdentity()->id );

        } else $this->_noAuth($request);
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        /** destroy the session **/
        $this->_logout();
    }

    protected function _initSession()
    {
        $defaultNamespace = new Zend_Session_Namespace ;
        if ( ! $defaultNamespace->initialized )
        {
            Zend_Session::regenerateId();
            $defaultNamespace->initialized = true;
        }
    }

    protected function _logout()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $namespace = new Zend_Session_Namespace('LoginData');
        $namespace->unlock();
        $namespace->unsetAll();

        $namespace = new Zend_Session_Namespace('Autenticacion');
        $namespace->unlock();
        $namespace->unsetAll();
    }

    protected function _initAdapter ($username, $password)
    {
        // setup Zend_Auth adapter for a database table
        $dbAdapter = Zend_Registry::get('db');

        $authAdapter = 
            new Restfulapi_Auth_Adapter_DbTable (
                $dbAdapter,
                'users',
                'username',
                'password'
            );

        /** Set the input credential values to authenticate against **/
        $authAdapter->setIdentity($username);
        $authAdapter->setCredential($password);
        return $authAdapter;
    }

    /**
     * Redirects users to our no authentication error page.
     *
     * @param \Zend_Controller_Request_Abstract $request
     * @return mixed
     */
    protected function _noAuth(Zend_Controller_Request_Abstract $request)
    {
        echo 'no auth' ;
        exit;
    }

    /**
     * Redirects users to our invalid request error page.
     *
     * @param \Zend_Controller_Request_Abstract $request
     * @return mixed
     */
    protected function _invalidRequest(Zend_Controller_Request_Abstract $request)
    {
        echo 'invalid' ;
        exit;
    }
}
