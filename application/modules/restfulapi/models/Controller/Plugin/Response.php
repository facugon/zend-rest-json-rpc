<?php

class Restfulapi_Controller_Plugin_Response extends Zend_Controller_Plugin_Abstract
{
    /** construyo la respuesta adecuada segun el metodo de comunicacion solicitado **/
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        $response = $this->getResponse();
        if( !$response->isException() && is_null($request->getParam('error_handler')) ) /** no setea el response si hay un error **/
        {
            if( ! Rest_JsonRpc_Server::IsHandshakeRequest() ) /** por ahora solo utilizo JSON RPC **/
            {
                $front      = Zend_Controller_Front::getInstance();
                $response = $front->getParam('bootstrap')->getResource('view');

                $request = Rest_JsonRpc_Server::GetInstance()->getRequest();
                $response->id = $request->getId() ;

                echo $response->render('jsonrpc_response_success.phtml');
            }
        }
    }
}
