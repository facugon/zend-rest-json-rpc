<?php

require_once 'Controller/Rest.php' ;

class Restfulapi_TaxiController extends Restfulapi_Controller_Rest
{
    public function indexAction() {
    }

    public function getAction()
    {
        require_once 'Taxis/DAO/Read/Viaje.php' ;
        $dao = new Taxis_DAO_Read_Viaje ;

        //$this->view->result = array ( 'taxi' => $this->_getParam('id') ) ;

        $this->result = $dao->obtViaje( $this->_request->getParam('id') );
    }

    /**
     * este metodo es como un get, pero solo tiene que responder los headers html.
     * es una forma de hacer un discover de recursos.
     * se podría ignorar o no permitir por seguridad
     **/
    public function headAction() {
    }

    public function postAction()
    {

        $request = Mbi_Rest_JsonRpc_Server::GetInstance()->getRequest();
        $params = $request->getParams() ;

        require_once 'Taxis/Service/Gestion/Creacion.php' ;
        $servCreacion = new Taxis_Service_Gestion_Creacion;
        $dataTicket = $servCreacion->generarTicket($params);

        if($dataTicket['codigo'] == $dataTicket['padre'])
        {
            Zend_Loader::loadClass('Taxis_Service_Email_Viaje_Simple_Creacion');
            $servEmailCreacion = new Taxis_Service_Email_Viaje_Simple_Creacion($dataTicket);
        }
        else
        {
            Zend_Loader::loadClass('Taxis_Service_Email_Viaje_Combinado_Creacion');
            $servEmailCreacion = new Taxis_Service_Email_Viaje_Combinado_Creacion($dataTicket);
        }
        $servEmailCreacion->procesarEnvioCompleto();

        $translate = Zend_Registry::get('language');
        $language = Zend_Registry::get('userlanguage');

        $this->response->result = $translate->_('textosolicitudingresada', $language);
    }

    public function putAction(){
    }

    public function deleteAction(){
    }

    public function optionsAction() {
        $this->render(); // render options.jsonrpc.phtml script
    }
}
