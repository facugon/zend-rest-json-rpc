<?php

require_once 'Controller/Rest.php' ;

class Restfulapi_IndexController extends Restfulapi_Controller_Rest
{
    public function indexAction()
    {
        $this->view->result = array('payload' => 'lista de recursos');
        $this->view->id = 0;
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function getAction()
    {
        $this->getResponse()->setHttpResponseCode(200);
        $this->view->result = array(
            'muerto' => $this->_request->getParam('id')
        );
    }

    public function postAction()
    {
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction()
    {
        $this->getResponse()->setHttpResponseCode(201);

        $this->view->result = 'new resource created';
        $this->view->id     = $this->_getParam('id');
    }

    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function optionsAction()
    {
        $this->render();
    }

    public function headAction()
    {
    }
}
