<?php
/**
 * RESTful Zend Framework
 *
 * @copyright  Copyright (c) 2011 Code in Chaos Inc. (http://www.codeinchaos.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id$
 *
 * standard Zend Application generated ErrorController
 * with Zend_Rest_Controller inheritance
 */
require_once 'Controller/Rest.php' ;

class Restfulapi_ErrorController extends Restfulapi_Controller_Rest
{
    public function init()
    {
    }

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
        case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
        case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
        case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
            /** 404 error -- controller or action not found **/
            $this->getResponse()->setHttpResponseCode(404);
            break;
        default:
            /** application error **/
            $this->getResponse()->setHttpResponseCode(500);
            break;
        }

        $this->view->error = array(
            'message' => $errors->exception->getMessage(),
            'code'    => $errors->exception->getCode()
        );
    }

    /**
     * Index Action
     *
     * @return void
     */
    public function indexAction()
    {
        echo 'index' ;
    }

    /**
     * GET Action
     *
     * @return void
     */
    public function getAction()
    {
    }

    /**
     * POST Action
     *
     * @return void
     */
    public function postAction()
    {
    }

    /**
     * PUT Action
     *
     * @return void
     */
    public function putAction()
    {
    }

    /**
     * DELETE Action
     *
     * @return void
     */
    public function deleteAction()
    {
    }

    /**
     * OPTIONS Action
     *
     * @return void
     */
    public function optionsAction()
    {
    }

    /**
     * HEAD Action
     *
     * @return void
     */
    public function headAction()
    {
    }
}
