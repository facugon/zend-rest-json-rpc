<?php

abstract class Rest_JsonRpc
{
    static private $Request_Header = array(
        'jsonrpc' => '2.0',
        'method'  => null,
        'params'  => null,
        'id'      => null
    );

    static private $Response_Header_Success = array(
        'jsonrpc' => '2.0',
        'result'  => null,
        'id'      => null
    );

    static private $Response_Header_Error = array(
        'jsonrpc' => '2.0',
        'error'  => null,
        'id'      => null
    );

    static public function GetRequestHeader() {
        return self::$Request_Header ;
    }

    static public function GetResponseHeaderSuccess() {
        return self::$Request_Header_Success ;
    }

    static public function GetResponseHeaderError() {
        return self::$Request_Header_Error ;
    }
}
