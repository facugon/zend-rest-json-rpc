<?php
/**
 *
 * Constructor de Request para servicio Rest con payload Json-RPC
 *
 */
class Rest_Client
{
    protected $url;
    protected $method;
    protected $requestBody;
    protected $requestData;
    protected $requestLength;
    protected $username;
    protected $password;
    protected $responseBody;
    protected $responseInfo;

    /** HTTP Headers **/
    protected $acceptType;
    protected $contentType;

    /**
     * @param string $url the api url
     * @param string $method the request http method
     * @param array $requestData data to be send to the api
     */
    public function __construct ($url = null, $method = 'GET', $requestData = null)
    {
        $this->url           = $url;
        $this->method        = $method;
        $this->requestData   = $requestData;
        $this->requestLength = 0;
        $this->username      = null;
        $this->password      = null;
        $this->acceptType    = 'application/json';
        $this->contentType   = 'application/json';
        $this->responseBody  = null;
        $this->responseInfo  = null;

        if ($this->requestData !== null)
        {
            $this->_buildRequestBody();
        }
    }

    public function flush ()
    {
        $this->requestLength = 0;
        $this->requestBody   = null;
        $this->responseBody  = null;
        $this->responseInfo  = null;
        return $this ;
    }

    public function send ()
    {
        $ch = curl_init();
        //$this->_setAuth($ch);

        try
        {
            switch ( strtoupper($this->method) )
            {
            case 'GET':
                $this->_get($ch);
                break;
            case 'POST':
                $this->_post($ch);
                break;
            case 'PUT':
                $this->_put($ch);
                break;
            case 'DELETE':
                $this->_delete($ch);
                break;
            case 'HEAD':
                $this->_head($ch);
                break;
            case 'OPTIONS':
                $this->_options($ch);
                break;
            default:
                throw new InvalidArgumentException("Current method ({$this->method}) is an invalid REST method.");
            }
        }
        catch (InvalidArgumentException $e)
        {
            curl_close($ch);
            throw $e;
        }
        catch (Exception $e)
        {
            curl_close($ch);
            throw $e;
        }

        return $this ;
    }

    protected function _getHttpHeaders()
    {
        $length = strlen($this->requestBody) ;

        return array (
            "Accept: {$this->acceptType}",
            "Content-Type: {$this->contentType}",
            "Content-Length: {$length}",
        ) ;
    }

    protected function _buildRequestBody ()
    {
        $data = $this->requestData ;

        if (!is_array($data))
        {
            throw new InvalidArgumentException('Invalid data input for postBody.  Array expected');
        }

        $data = http_build_query($data, '', '&');
        $this->setRequestBody($data);

        return $this ;
    }

    protected function _get ($ch)
    {
        $this->_doExecute($ch);  
        return $this ;
    }

    protected function _post ($ch)
    {
        if ( ! is_string($this->requestBody) )
        {
            $this->_buildRequestBody();
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        $this->_doExecute($ch);  
        return $this ;
    }

    protected function _put ($ch)
    {
        if (!is_string($this->requestBody))
        {
            $this->_buildRequestBody();
        }

        $fh = fopen('php://memory', 'rw');
        fwrite($fh, $this->requestBody);
        rewind($fh);

        curl_setopt($ch, CURLOPT_INFILE, $fh);
        curl_setopt($ch, CURLOPT_INFILESIZE, $this->requestLength);
        curl_setopt($ch, CURLOPT_PUT, true);

        $this->_doExecute($ch);
        fclose($fh);
        return $this ;
    }

    protected function _delete ($ch) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

        $this->_doExecute($ch);
        return $this ;
    }

    protected function _head ($ch) {
        throw new BadMethodCallException('Method "head" is not supported, yet.');
        return $this ;
    }

    protected function _options ($ch) {
        throw new BadMethodCallException('Method "options" is not supported, yet.');
        return $this ;
    }

    protected function _doExecute (&$curlHandle)
    {
        $this->_setCurlOpts($curlHandle);
        $this->responseBody = curl_exec($curlHandle);
        $this->responseInfo = curl_getinfo($curlHandle);

        curl_close($curlHandle);
        return $this ;
    }

    protected function _setCurlOpts (&$curlHandle) {
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 10);
        curl_setopt($curlHandle, CURLOPT_URL, $this->url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $this->_getHttpHeaders() );

        return $this ;
    }

    protected function _setAuth (&$curlHandle)
    {
        if ($this->username !== null && $this->password !== null)
        {
            curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
            curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        }
        return $this ;
    }

    public function getAcceptType () {
        return $this->acceptType;
    } 

    public function setAcceptType ($acceptType) {
        $this->acceptType = $acceptType;
        return $this ;
    } 

    public function getPassword () {
        return $this->password;
    } 

    public function setPassword ($password) {
        $this->password = $password;
        return $this ;
    } 

    public function getResponseBody () {
        return $this->responseBody;
    } 

    public function getResponseInfo () {
        return $this->responseInfo;
    } 

    public function getUrl () {
        return $this->url;
    } 

    public function setUrl ($url) {
        $this->url = $url;
        return $this ;
    } 

    public function getUsername () {
        return $this->username;
    } 

    public function setUsername ($username) {
        $this->username = $username;
        return $this ;
    } 

    public function setMethod ($method) {
        $this->method = $method;
        return $this ;
    } 

    public function setRequestData($data) {
        $this->requestData = $data ;
        return $this ;
    }

    public function getMethod () {
        return $this->method;
    } 

    public function getRequestData() { 
        return $this->requestData ;
    }

    public function debug(){
        echo '<pre>' . print_r($this, true) . '</pre>'; 
    }

    final public function setRequestBody($rb)
    {
        $this->requestBody   = $rb ;
        $this->requestLength = strlen($this->requestBody);
    }
}
