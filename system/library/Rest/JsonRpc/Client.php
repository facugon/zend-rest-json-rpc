<?php

/**
 *
 * Constructor de Request para servicio Rest con payload Json-RPC
 *
 */
require_once 'Rest/JsonRpc.php' ;

class Rest_JsonRpc_Client extends Rest_Client
{
    private function _requestId()
    {
        return md5(serialize($this->requestData));
    }

	protected function _buildRequestBody ()
	{
		$data = $this->requestData ;

        if (!is_array($data))
        {
            throw new InvalidArgumentException('Invalid data input for postBody. Array expected');
        }

        $body = Rest_JsonRpc::GetRequestHeader() ;

        $body['method'] = $this->method ;
        $body['id']     = $this->_requestId() ;
        $body['params'] = $data ;

        $this->setRequestBody( json_encode($body) );

		return $this ;
	}
}
