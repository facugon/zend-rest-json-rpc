<?php

/** singleton **/
class Rest_JsonRpc_Server
{
    const ENV_JSONRPC_V  = '2.0' ;

    /**
     *
     * Protocol defined errors.
     * The error codes from and including -32000 to -32768 are reserved for pre-defined errors
     *
     * @link http://www.jsonrpc.org/specification specification
     *
     **/
    const ParseError           = -32700;
    const InvalidRequestObject = -32600;
    const MethodNotFound       = -32601;
    const InvalidParameters    = -32602;
    const InternalJsonRpcError = -32603;

    /** Custom errors **/
    const VersionError         = -30000;

    protected $_errors = array (
        self::VersionError         => 'JSON Version not suported.',
        self::ParseError           => 'Invalid JSON was received by the server. Not well formed.',
        self::InvalidRequestObject => 'The JSON sent is not a valid Request object.',
        self::MethodNotFound       => 'The method does not exist / is not available.',
        self::InvalidParameters    => 'Invalid method parameter(s).',
        self::InternalJsonRpcError => 'Internal JSON-RPC error.',
    );

    protected $_request  = null ; /** Zend_Json_Server_Request_Http **/
    protected $_response = null ; /** Zend_Json_Server_Response     **/

    static private $instance = null ;

    static public function GetInstance ()
    {
        if( !is_object(self::$instance) )
        {
            $c = __CLASS__;
            $instance = new $c();
            $instance->setRequest();

            self::$instance = $instance ;
        }
        return self::$instance;
    }

    /** estos metodos no vienen con parametros **/
    static public function IsHandshakeRequest()
    {
        $_comuMethods = array('HEAD','OPTIONS') ;

        return in_array( $_SERVER['REQUEST_METHOD'], $_comuMethods );
    }

    /** estos metodos vienen con parametros en formato JsonRpc2 estrictamente **/
    static public function StrictJsonHandshakeRequired ()
    {
        $_jsonMethods = array('POST','PUT'); // this methods should include information to update/insert resources always
    }

    public function setRequest ()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest() ;

        $jsonRequest = new Zend_Json_Server_Request_Http ;
        $this->_request = $jsonRequest ;

        if( ! self::IsHandshakeRequest() )
        {
            if( self::StrictJsonHandshakeRequired() )
            {
                if( ! $this->_validVersion() )
                    throw new Exception($this->_errors[self::VersionError], self::VersionError) ;

                if( ! $this->_validMethod() )
                    throw new Exception($this->_errors[self::MethodNotFound], self::MethodNotFound);
            }
        }

        return $this ;
    }

    public function getRequest() {
        return $this->_request ;
    }

    protected function _validMethod()
    {
        return true ; // is : index, get, post, put, delete, head, options
    }

    protected function _validVersion()
    {
        return $this->_request->getVersion() == self::ENV_JSONRPC_V ;
    }
}
