<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Dojo
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: BuildLayer.php 23775 2011-03-01 17:25:24Z ralph $
 */

/**
 * Dojo module layer and custom build profile generation support
 *
 * @package    Zend_Dojo
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_Dojo_BuildLayer
{
    /**
     * Flag: whether or not to consume JS aggregated in the dojo() view
     * helper when generate the module layer contents
     * @var bool
     */
    protected $_consumeJavascript = false;

    /**
     * Flag: whether or not to consume dojo.addOnLoad events registered
     * with the dojo() view helper when generating the module layer file
     * contents
     * @var bool
     */
    protected $_consumeOnLoad = false;

    /**
     * Dojo view helper reference
     * @var Zend_Dojo_View_Helper_Dojo_Container
     */
    protected $_dojo;

    /**
     * Name of the custom layer to generate
     * @var string
     */
    protected $_layerName;

    /**
     * Path to the custom layer script relative to dojo.js (used when
     * creating the build profile)
     * @var string
     */
    protected $_layerScriptPath;

