<?php

/** Base sources and global variable definitions **/

defined('APPLICATION_ENV')  || define('APPLICATION_ENV'  , (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

defined('BASE_PATH')        || define('BASE_PATH'        , realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . '..' );

defined('PUBLIC_PATH')      || define('PUBLIC_PATH'      , BASE_PATH . '/public'         ); 
defined('LIB_PATH')         || define('LIB_PATH'         , BASE_PATH . '/system/library' ); 
defined('CORE_PATH')        || define('CORE_PATH'        , BASE_PATH . '/'               ); 
defined('APPLICATION_PATH') || define('APPLICATION_PATH' , BASE_PATH . '/application'    ); 


defined('PUBLIC_COMMON_PATH')   || define('PUBLIC_COMMON_PATH'   , PUBLIC_PATH . '/common/' );

/**
 * Customer definition , multiple customers application structure
 *
 * defined('CUSTOMER_ENV')     || define('CUSTOMER_ENV'     , (getenv('CUSTOMER_ENV')    ? getenv('CUSTOMER_ENV')    : 'default'));
 * defined('CUSTOMERS_PATH')   || define('CUSTOMERS_PATH'   , BASE_PATH . '/customers'      ); 
 * defined('CUSTOMER_PATH')    || define('CUSTOMER_PATH'    , CUSTOMERS_PATH . DIRECTORY_SEPARATOR . CUSTOMER_ENV );
 * defined('PUBLIC_CUSTOMER_PATH') || define('PUBLIC_CUSTOMER_PATH' , PUBLIC_PATH . DIRECTORY_SEPARATOR . CUSTOMER_ENV );
 *
 */


define('CLI_APPLICATION', false);
