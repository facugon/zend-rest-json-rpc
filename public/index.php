<?php

require_once realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'gralDef.php' ;

// Set include path to Zend (and other) libraries
set_include_path (
    LIB_PATH .
    PATH_SEPARATOR . APPLICATION_PATH .
    PATH_SEPARATOR . APPLICATION_PATH . '/models' .
    PATH_SEPARATOR . APPLICATION_PATH . '/modules/restfulapi/models' .
    PATH_SEPARATOR . get_include_path() .
    PATH_SEPARATOR . '.'
);

require_once 'Zend/Application.php';
require_once 'Rest/WebService/Definitions.php' ;

define('API_PAYLOAD'     , ENV_JSONRPC_20);
define('API_MODULE'      , 'restfulapi');
define('API_CONFIG_FILE' , '/data/configs/api.ini');

// Create application, bootstrap, and run
$application = new Zend_Application (
    APPLICATION_ENV ,
    APPLICATION_PATH . API_CONFIG_FILE
);

$application->bootstrap()->run();
